<?php


class Db_object
{

    protected static $db_table;
    protected static $db_table_fields;


    //get all
    public static function find_all()
    {
        global $database;

        $query = 'SELECT * FROM  ' . static::$db_table;

        return static::queryObject($query);
    }

    //get one
    public static function find_by_id($id)
    {
        $query = 'SELECT * FROM ' . static::$db_table . ' WHERE id =' . $id;
        return static::first(static::queryObject($query));
    }

    //get first value from array of objects
    protected static function first($array)
    {
        if (isset($array[0])) {
            return $array[0];
        } else {
            return false;
        }
    }

    //return array of objects of current class
    public static function queryObject($query)
    {
        global $database;
        $object_array = [];
        $data = $database->sqlQuery($query);

        while ($row = $data->fetch_assoc()) {
            array_push($object_array, static::instantiateObject($row)); //pushing instantiate object in array
        }

        return $object_array;
    }


    //instantiate object from passed data
    private static function instantiateObject($data)
    {
        $object = new static;

        foreach ($data as $property => $value) {
            if (static::has_the_property($property)) {
                $object->$property = $value;
            }
        }

        return $object;
    }

    //check if property exists
    private static function has_the_property($property)
    {
        $object_properties = get_object_vars((new static));

        return array_key_exists($property, $object_properties);
    }

    //return array of properties and properties values
    protected function properties()
    {
        global $database;
        $properties = array();

        foreach (static::$db_table_fields as $field) {
            if (property_exists($this, $field)) {
                $properties[$field] = $database->real_escape_string($this->$field);
            }
        }
        return $properties;
    }

    //check if record (object) needs to be created or updated
    //if object id exists, method update existed record (object)
    public function save()
    {
        return isset($this->id) ? $this->update() : $this->create();
    }


    //create method 
    public function create()
    {

        global $database;
        $properties = $this->properties(); //getting all properties of current class



        $query = "INSERT INTO " . static::$db_table . " (" . implode(",", array_keys($properties)) . ")
        VALUES ('" . implode("','", array_values($properties)) . "')";



        if ($database->sqlQuery($query)) {
            $this->id = $database->the_insert_id();
            return true;
        } else {
            return false;
        }
    }



    //update method
    public function update()
    {
        global $database;

        $properties_pairs = $this->prepare_data_for_update($this->properties());

        $query = "UPDATE  " . static::$db_table . " SET " . implode(", ", $properties_pairs) . "WHERE id =" . $database->real_escape_string($this->id);

        $database->sqlQuery($query);

        return ($database->affected_rows == 1) ? true : false;
    }

    //prepare data for update method
    protected function prepare_data_for_update($properties)
    {
        $properties_pairs = array();

        foreach ($properties as $key => $value) {
            $properties_pairs[] = "{$key} = '{$value}'";
        }

        return $properties_pairs;
    }


    //delete method
    public function delete()
    {
        global $database;

        $query = "DELETE FROM " . static::$db_table . " WHERE id='" . $database->real_escape_string($this->id) . "' LIMIT 1";

        $database->sqlQuery($query);

        return ($database->affected_rows == 1) ? true : false;
    }
}//End of Db_object class
