<?php


class Database extends mysqli
{

    //instantiate parent constructor
    function __construct()
    {

        parent::__construct(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);
    }

    //query run function
    public function sqlQuery($query)
    {
        $data = $this->query($query);

        $this->check_query_error($data);

        return $data;
    }

    //need upgrade
    //return Sql errors
    private function check_query_error($data)
    {

        if ($this->errno) {
            die('Sql error: ' . $this->errno . ' >>> ' . $this->error);
        }
    }

    //get last id after query
    public function the_insert_id()
    {
        return $this->insert_id;
    }
} //End of class Database
$database = new Database();
