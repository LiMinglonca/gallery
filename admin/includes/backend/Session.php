<?php

class Session
{

    public $user_id;
    private $signed_in = false;
    public $message;

    public function message($msg=''){

        if(!empty($msg)){
            $_SESSION['message'] = $msg;
        }
        else{
            return $this->message;
        }

    }

    public function check_message(){
        if(isset($_SESSION['message'])){
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);

            return $this->message;
        }
        else{
            return $this->message="";

        }
    }

    function __construct()
    {
        session_start();
        $this->check_the_login();
        $this->check_message();
    }

    public function is_signed_in()
    {
        return $this->signed_in;
    }

    public function logout(){
        $this->signed_in = false;
        unset($user_id);
        unset($_SESSION['user_id']);
    }

    public function login(User $user)
    {
       
        $this->user_id = $user->id;
        $this->signed_in = true;
        $_SESSION['user_id'] = $user->id;

    }

    


    private function check_the_login(){
        if(isset($_SESSION['user_id'])){
            $this->user_id = $_SESSION['user_id'];
            $this->signed_in = true;
        }
        else{
            unset($this->user_id);
            $this->signed_in = false;
        }
    }





   
    
    
   
}



$session = new Session();
