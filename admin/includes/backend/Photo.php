<?php


class Photo extends Db_object
{

    protected static $db_table = 'photos';
    protected static $db_table_fields = array('title', 'description', 'size', 'type','filename');

    public $id;
    public $title;
    public $description;
    public $type;
    public $size;
}
