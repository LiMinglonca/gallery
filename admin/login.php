<?php require_once('includes/frontend/header.php'); ?>

<?php


if($session->is_signed_in()){
    redirect('index.php');
}

if (isset($_POST['login'])) {
	$username = trim($_POST['username']);
	$password = trim($_POST['password']);

	if (empty($username) && empty($password)) {
		$the_message = "Credintials is empty";
	} else {
		if (User::check_user($username, $password)) {
			$session->login(User::check_user($username, $password));
			redirect('index.php'); //make confi

		} else {
			$the_message = "Erro while login";
		}
	}
}

?>

<div class="col-md-4 col-md-offset-3">

	<h4 class="bg-danger"><?php if (isset($the_message)) {
								echo $the_message;
							} ?></h4>

	<form id="login-id" action="" method="post">

		<div class="form-group">
			<label for="username">Username</label>
			<input type="text" class="form-control" name="username" value="<?php if (isset($username)) {
																				echo htmlentities($username);
																			} ?>">

		</div>

		<div class="form-group">
			<label for="password">Password</label>
			<input type="password" class="form-control" name="password" value="<?php if (isset($password)) {
																					echo htmlentities($password);
																				} ?>">

		</div>


		<div class="form-group">
			<input type="submit" name="login" value="Submit" class="btn btn-primary">

		</div>


	</form>


</div>