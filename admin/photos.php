<?php include("includes/frontend/header.php"); ?>
<?php
if (!$session->is_signed_in()) {
    redirect('login.php');
}
?>

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <?php include_once('includes/frontend/top_nav.php'); ?>
    <?php include_once('includes/frontend/side_nav.php'); ?>

</nav>

<!-- page content -->
<div id="page-wrapper">

    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <!-----------------------------------------Testing here -------------------------------------------------------------------->

                <?php
                      $photo = new Photo();

                      $photo->title='prva slika ikad';
                      $photo->description='ikad prva slika';
                      $photo->size=124;
                      $photo->type='jpg';
                      $photo->filename='skila123.jpg';

                      $photo->save();
                ?>
                    <!------------------------------------------------------------------------------------------------------------------------------>
                    <small>Photos page</small>
                </h1>
                <ol class="breadcrumb">
                    <li>
                        <i class="fa fa-dashboard"></i> <a href="index.html">Dashboard</a>
                    </li>
                    <li class="active">
                        <i class="fa fa-file"></i> Blank Page
                    </li>
                </ol>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

<?php include("includes/frontend/footer.php"); ?>